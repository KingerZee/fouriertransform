#include "stdafx.h"
#include "FFT_2d.h"
FFT_2d::FFT_2d(void)
{
	m_fft_matrix = NULL;
	m_spectrum_data = NULL;
	m_has_fft_matrix = false;
	m_is_normalized = false;
	m_is_spectrum_shifted = false;
	m_fft_matrix_height = 0;
	m_fft_matrix_width = 0;
}
FFT_2d::~FFT_2d(void)
{
	if(m_has_fft_matrix && (NULL != m_fft_matrix) &&((m_fft_matrix_height*m_fft_matrix_width)>0))
		delete [] m_fft_matrix;
	if(NULL != m_spectrum_data)
		delete [] m_spectrum_data;
}

bool FFT_2d::has_fft_matrix()
{
	return m_has_fft_matrix;
}
bool FFT_2d::is_shifted_to_center()
{
	return m_is_spectrum_shifted;
}

void FFT_2d::clear_fft_matrix()
{
	if(m_has_fft_matrix && (NULL != m_fft_matrix) && ((m_fft_matrix_height*m_fft_matrix_width)>0))
	{
		delete [] m_fft_matrix;
		m_has_fft_matrix = false;
		m_fft_matrix_height = 0;
		m_fft_matrix_width = 0;
	}
}

void FFT_2d::print_matrix()
{

}

void FFT_2d::printf_spectrum()
{

}

bool FFT_2d::FFT(double IN const matrix[],int IN const width,int IN const height)
{
	if(((width*height)<=0)|| ((width*height)>MAX_MATRIX_SIZE)||(NULL == matrix))
		return false;
	clear_fft_matrix();
	m_fft_matrix = new ComplexNumber[width*height];
	ComplexNumber cplTemp(0,0);
	double fixed_factor_for_axisX = (-2*PI)/height;
	double fixed_factor_for_axisY = (-2*PI)/width;
	for(int u = 0;v < height;u++){
		for(int v = 0;v < width;v++){
			for(int x = 0;x < height;x++){
				for(int y = 0; y < width;y++){
					double powerX = u*x*fixed_factor_for_axisX;
					double powerY = v*y*fixed_factor_for_axisY;
					cplTemp.m_rl = matrix[y+x*width]*cos(powerX+powerY);
					cplTemp.m_im = matrix[y+x*width]*sin(powerX+powerY);
					m_fft_matrix[v + u*width] = m_fft_matrix[v+u*width]+cplTemp;
				}
			}

		}
	}
	m_has_fft_matrix = true;
	m_fft_matrix_height = height;
	m_fft_matrix_width = width;
	return true;
}

void FFT_2d::generate_spectrum()
{
	if(m_has_fft_matrix && (NULL != m_fft_matrix)&&((m_fft_matrix_width*m_fft_matrix_height)>0)){
		if(NULL != m_spectrum_data){
			delete[] m_spectrum_data;
			m_is_spectrum_shifted = false;
		}
		m_spectrum_data = new double[m_fft_matrix_height*m_fft_matrix_width];
		for(int u = 0; u < m_fft_matrix_height; u++){
			for(int v = 0;v <m_fft_matrix_width;v++){
				double a = m_fft_matrix[v+u*m_fft_matrix_width].m_rl * m_fft_matrix[v+u*m_fft_matrix_width].m_rl;
				double b = m_fft_matrix[v+u*m_fft_matrix_width].m_im * m_fft_matrix[v+u*m_fft_matrix_width].m_im;
				m_spectrum_data[v+u*m_fft_matrix_width] = sqrt(a+b);
				
			}
		}
	}
}

void FFT_2d::normalize_spectrum()
{
	if(m_has_fft_matrix && (NULL != m_fft_matrix) &&((m_fft_matrix_height*m_fft_matrix_width)>0)&&((NULL != m_spectrum_data))){
		double max = 0;
		for(int u=0;u<m_fft_matrix_height;u++){
			for(int v=0;v<m_fft_matrix_width;v++){
				if(m_spectrum_data[v+u*m_fft_matrix_width]>max){
					max = m_spectrum_data[v+u*m_fft_matrix_width];
				}
			}
		}
		if(max<=0){
			return ;
		}
		else{
			for(int u=0; u<m_fft_matrix_height;u++){
				for(int v=0;v<m_fft_matrix_width;v++){
					m_spectrum_data[v+u*m_fft_matrix_width]=(255 * m_spectrum_data[v+u*m_fft_matrix_width])/max;

				}
			}
		}
		m_is_normalized = true;
	}
	else
		m_is_normalized = false;
}

void FFT_2d::shift_spectrum_to_center()
{
	if(m_is_spectrum_shifted)
		return ;
	if(m_has_fft_matrix && (NULL != m_fft_matrix) &&((m_fft_matrix_height*m_fft_matrix_width)>0)&&((NULL != m_spectrum_data))){
		double *tempData = new double[m_fft_matrix_height*m_fft_matrix_width];
		memcpy(tempData,m_spectrum_data,m_fft_matrix_height*m_fft_matrix_width*sizeof(double));
		for(int u=0; u<m_fft_matrix_height;u++){
			for(int v=0; v< m_fft_matrix_width;v++){
				if((u<(m_fft_matrix_height/2))&&(v<(m_fft_matrix_width/2))){
					m_spectrum_data[v+u*m_fft_matrix_width] = tempData[m_fft_matrix_width/2 + v + (m_fft_matrix_height/2 + u) * m_fft_matrix_width];

				}
				else if((u<(m_fft_matrix_height/2))&&(v >= (m_fft_matrix_width/2))){
					m_spectrum_data[v+u*m_fft_matrix_width] = tempData[( v - m_fft_matrix_width/2 ) + (m_fft_matrix_height/2 + u) * m_fft_matrix_width];
	
				}
				else if((u>(m_fft_matrix_height/2))&&(v < (m_fft_matrix_width/2))){
					m_spectrum_data[v+u*m_fft_matrix_width] = tempData[ m_fft_matrix_width/2 + v + (u - m_fft_matrix_height/2 ) * m_fft_matrix_width];
	
				}
				else if((u >= (m_fft_matrix_height/2))&&(v >= (m_fft_matrix_width/2))){
					m_spectrum_data[v+u*m_fft_matrix_width] = tempData[( v - m_fft_matrix_width/2 ) + (u - m_fft_matrix_height/2) * m_fft_matrix_width];
	
				}
			}
		}
		m_is_spectrum_shifted = true;
		if(tempData)
			delete[] tempData;
	}
}

