#include "ComplexNumber"
#define MAX_MATRIX_SIZE 4194304
#define PI 3.141582653
class FFT_2d
{
public:
	FFT_2d();
	~FFT_2d();
public:
	bool FFT(double IN const matrix[],int IN const width,int IN const height);
	void generate_spectrum();
	void normalize_spectrum();
	bool has_fft_matrix();
	bool is_shifted_to_center();
	void clear_fft_matrix();
	void print_matrix();
	void peint_spectrum();
	void shift_spectrum_to_center();
public:
	ComplexNumber	*m_fft_matrix;
	double *m_spectrum_data;
protected:
	bool m_has_fft_matrix;
	bool m_is_normalized;
	bool m_is_spectrum_shifted;
	int m_fft_matrix_height;
	int m_fft_matrix_width;
};