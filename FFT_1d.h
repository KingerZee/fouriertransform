#define MAX_MATRIX_SIZE 4194304
#define PI 3.14159265
#define MAX_VECTOR_LENGTH 10000
typedef struct Complex
{
	double rl;
	double im;
	
}Complex;

class FFT_1d
{
public:
	FFT_1d();
	~FFT_1d();
public:
	bool FFT(Complex IN const inVec[],int IN const length,Complex OUT outVec[]);
	bool is_power_of_two(int IN num);
	int get_computation_layers(int IN num);
	
};