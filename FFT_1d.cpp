#include "stdafx.h"
#include "FFT_1d.h"
FFT_1d::FFT_1d()
{

}
FFT_1d::~FFT_1d()
{

}

bool FFT_1d::is_power_of_two(int IN num){
	int tmp = num;
	int mod = 0;
	int result = 0;
	if(num<2)
		return false;
	if(num == 2)
		return true;
	while(tmp>1)
	{
		result = tmp/2;
		mod = tmp %2;
		if(mod)
			return false;
		if(2==result)
			return true;
		tmp = result;
	}
	return false;
}

int FFT_1d::get_computation_layers(int IN num)
{
	int nLayers = 0;
	int len = num;
	if (len == 1)
		return 1;
	while(true){
		len = len/2;
		nLayers ++;
		if(len == 2)
			return nLayers + 1;
		if(len <1)
			return -1;
	}
}

bool FFT_1d::FFT(Complex IN inVec[],int IN length,Complex OUT outVec[])
{
	if((length <= 0)||(NULL == inVec))
		return false;
	if(!is_power_of_two(length))
		return false;
	// pVec is an array to save input signal
	// weights is the coefficient weight matrix
	//X and pbInvBits are used for coordinate flip
	Complex *pVec = new Complex[length];
	Complex *weights = new Complex[length];
	Complex *X = new Complex[length];
	int *pnInvBits = new int[length];
	memcpy(pVec,inVec,length*sizeof(Complex));
	

	// Calculate the weight matrix
	double fixed_factor = (-2*PI)/length;
	for(int i = 0;i<length/2;i++){
		double angle = i *fixed_factor;
		weights[i].rl = cos(angle);
		weights[i].im = sin(angle);
	} 
	for(int i = length/2;i<length;i++){
		weights[i].rl = -(weights[i-length/2].rl);
		weights[i].im = -(weights[i-length/2].im);
	}

	int r = get_computation_layers(length);

	int index = 0;
	for(int i = 0; i < length;i ++){
		index = 0;
		for(int m = r-1;m>=0;m--){
			index +=(1&&(i&(1<<m)))<<(r-m-1);
		}
		pnInvBits[i] = index;
		X[i].rl = pVec[pnInvBits[i]].rl;
		X[i].im = pVec[pnInvBits[i]].im;
	}

	for(int L = 1; L<=r ; L++){
		int distance = 1<<(L-1);
		int W = 1 << (r-L);
		int B = length >> L;
		int N = length/B;

		for(int b = 0;b<B;b++){
			int mid = b*N;
			for(int n = 0;n<N/2;n++){
				int index = n + mid;
				int dist = index + distance;
				pVec[index].rl = X[index].rl + (weights[n*W].rl * X[dist].rl - weights[n*W].im * X[dist].im);
				pVec[index].im = X[index].im + weights[n*W].im * X[dist].rl - weights[n*W].rl * X[dist].im;
			}
			for(int n = N/2;n<N;n++){
				int index = n+ mid;
				pVec[index].rl = X[index-distance].rl+weights[n*W].rl*X[index].rl- weights[n*W].im * X[index].im;
				pVec[index].im = X[index-distance].im+weights[n*W].im*X[index].rl- weights[n*W].rl * X[index].im;
				
			}
		}
		memcpy(X,pVec,length*sizeof(Complex));
	}
	memcpy(outVec,pVec,length*sizeof(Complex));
	if(weights) delete[] weights;
	if(X)	delete[] X;
	if(pnInvBits) delete[] pnInvBits;
	if(pVec) delete[] pVec;
	return true;
}
